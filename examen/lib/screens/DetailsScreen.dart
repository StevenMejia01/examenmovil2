import 'package:flutter/material.dart';
import 'package:examen/computadora.dart';



class DetailScreen extends StatefulWidget {
  
    final Computadora computadora;
  
    final BuildContext context;
  
    const DetailScreen({Key? key, required this.computadora, required this.context}) : super(key: key);
  
    @override
  
    _DetailScreenState createState() => _DetailScreenState();
  
  }

class _DetailScreenState extends State<DetailScreen> {
    @override
    Widget build(BuildContext context) {
  return Scaffold(
  //SliverAppBar
  body: CustomScrollView(
    slivers: [
      SliverAppBar(
        //ocultar icono de volver
        automaticallyImplyLeading: false,
        expandedHeight: 300,
        flexibleSpace: FlexibleSpaceBar(
        
          background: Image.network(
            widget.computadora.imagen,
            fit: BoxFit.cover,
            height: 350,
          ),
        ),
      ),
      SliverList(
        delegate: SliverChildListDelegate(
          [
            Container(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Nombre de la computadora :' + '   '+
                          widget.computadora.nombre,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 5),
                        Text('Descripción general :' + '   '+
                          widget.computadora.descripcion,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                       
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Text(
                        widget.computadora.precio,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),

                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.all(16),
            child: TextButton(
                        //color del boton
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Color.fromARGB(255, 45, 106, 152),
                          onSurface: Color.fromARGB(255, 17, 16, 16),
                        ),
                        onPressed: () {
                           Navigator.pop(context);      
                         },
                        child: const Text('Done'),
               ),
            ),

          ],
        ),
      ),
    ],
  ),
);
}
}