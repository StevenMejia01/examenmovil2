import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:examen/backend.dart';
import 'package:examen/widgets/ComputadoraWidget.dart';


class ListScreen extends StatelessWidget {
     final Backend _backend;
  
  const ListScreen({Key? key, required Backend backend}) : _backend = backend, super(key: key);

  @override

  Widget build(BuildContext context) {
     SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    return Scaffold(
      body:ListView(
        children: _backend.getComputadoras().map((computadora) => ComputadoraWidget(computadora: computadora)).toList(),
      ),
    );
  }

}