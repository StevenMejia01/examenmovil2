import 'computadora.dart';
class Backend {
  /// Singleton pattern used here.
  static final Backend _backend = Backend._internal();

  var computadoras;

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  final _computadoras = [
    Computadora(
      id: 1,
      nombre: 'Dell OptiPlex',
      precio: '296.39',
      descripcion:'PC de escritorio Dell OptiPlex, Intel Core i5 de tercera generación a 3,2 GHz, 16 GB de RAM, disco duro de 2 TB, MTG, monitor LED de 22 pulgadas, teclado y ratón RGB, WiFi, Windows 10 Pro (renovado)',
      imagen: 'https://m.media-amazon.com/images/I/71qvwOcghiL._AC_SL1500_.jpg',
      ),
    Computadora(
      id: 2,
      nombre: 'Dell Inspiron',
      precio: '808.24',
      descripcion:'Dell Inspiron 24 5000 11th Gen Intel i5-1135G7 12GB 1TB HDD 256GB SSD 23.8-inch Full HD Touchscreen All-in-One PC',
      imagen: 'https://m.media-amazon.com/images/I/81rqI3pV6PL._AC_SL1500_.jpg',
      ),
    Computadora(
      id: 3,
      nombre: 'Apple iMac',
      precio: '490.19',
      descripcion:'(27 pulgadas, 8 GB, 1 TB de almacenamiento) Plateado (renovado)',
      imagen: 'https://m.media-amazon.com/images/I/71J3uJ-ZGJL._AC_SL1500_.jpg',
      ),
    Computadora(
      id: 4,
      nombre: 'Apple iMac 2017',
      precio: ' 850.15',
      descripcion:'Apple iMac 2017 con Intel Core i5 (21,5 pulgadas, 8 GB de RAM, 1 TB de almacenamiento) - Plateado (renovado)',
      imagen: 'https://m.media-amazon.com/images/I/61-Y+H8G6gL._AC_SL1200_.jpg',
      ),
    Computadora(
      id: 5,
      nombre: 'Apple iMac MF885LL/A',
      precio: '554.04',
      descripcion:'Apple iMac MF885LL/A Pantalla Retina 5K de 27 pulgadas (procesador de cuatro núcleos a 3,3 Ghz, disco duro de 1 TB, DDR3L de 8 GB) (renovada)',
      imagen: 'https://m.media-amazon.com/images/I/817BrKmuqkL._AC_SL1500_.jpg',
      ),
    
  ];

  ///
  /// Public API starts here :)
  ///

  /// Returns all products.
  List<Computadora> getComputadoras() {
    return _computadoras;
  }

  /// Marks products identified by its id as read.
  void markComputadorasRead(int id) {
    final index = _computadoras.indexWhere((computadora) => computadora.id == id);


  computadoratWidget(computadora) {}
  }
}
