import 'package:examen/backend.dart';
import 'package:examen/screens/ListScreen.dart';
import 'package:flutter/material.dart';


class appBarSTEVEN   extends StatelessWidget {
  const appBarSTEVEN({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = 'MEJIA ALVARADO STEVEN';
    return Scaffold(
      appBar: AppBar(
        title: Text('$name       8voA'),
        centerTitle: true,
      ),
      body: ListScreen(backend: Backend()),
    );
  }
}