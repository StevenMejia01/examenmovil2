import 'package:examen/screens/DetailsScreen.dart';
import 'package:flutter/material.dart';
import 'package:examen/computadora.dart';


class ComputadoraWidget extends StatelessWidget {
  final Computadora computadora;
  const ComputadoraWidget({Key? key, required this.computadora}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(  
      child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child:  Expanded(
             child: Column(
               children: [
                 GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailScreen(computadora: computadora, context:context),
                        ),
                      );
                    },
                    onLongPress: () {
                      print('Long Press');
                    },
                    child: Dismissible(
                     
                      key: UniqueKey(),
                      onDismissed: (direction) {
                        print('Dismissed');
                  },
                  child: Container(
                  padding: const EdgeInsets.all(8),
            child: Row(
                  children: [
                    Text(computadora.id.toString()),    
                    Container(
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(10.0),
                      decoration: const BoxDecoration(
                      color: Color.fromARGB(31, 0, 0, 0),
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          
                          Text(
                            computadora.nombre,
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 5),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10),
                    Column(
                      children: [
                        Text(
                          computadora.precio,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 5),
                      ]
                    ),
                  ],
            ),
          ),
        ),
      ),
    ],
  )
  )
  ));  
  }
}
